function reduce(elements, cb, initialvalue) {

    let accumulator = initialvalue !== undefined ? initialvalue : elements[0];
    let startindex = initialvalue !== undefined ? 0 : 1;

    for (let index = startindex; index < elements.length; index++) {
        accumulator = cb(accumulator, elements[index], index, elements);
    }

    return accumulator;
}

module.exports = reduce;