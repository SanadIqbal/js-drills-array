function find(elements, cb) {
    if (arguments.length !== 2 || Array.isArray(elements)) {
        return [];
    }
    for (let index = 0; index < elements.length; index++) {
        if (cb(elements[index], index, elements) === true) {
            return elements[index];
        }
    }
    return undefined;
}


module.exports = find;