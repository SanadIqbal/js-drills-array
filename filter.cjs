function filter(elements, cb) {
    if (arguments.length !== 2 || !Array.isArray(elements)) {
        return [];
    }
    const result = [];
    for (let index = 0; index < elements.length; index++) {
        if (cb(elements[index], index, elements) === true) {
            result.push(elements[index]);
        }
    }
    return result;
}


module.exports = filter;
