function map(elements, cb) {

    if (arguments.length !== 2 || !Array.isArray(elements)) {
        return [];
    }

    const result = [];

    for (let index = 0; index < elements.length; index++) {
        result.push(cb(elements[index], index,elements));
    }

    return result;
}

module.exports = map;