function flatten(elements, depth = 1) {
    if (!Array.isArray(elements)) {
        return [];
    }
    if (depth === 0) {
        return elements;
    }

    let result = [];

    for (let index = 0; index < elements.length; index++) {
        if (Array.isArray(elements[index])) {
            result = result.concat(flatten(elements[index], depth - 1));
        }
        else if (elements[index] === undefined) {
            continue;
        }
        else {
            result.push(elements[index]);
        }
    }

    return result;
}

module.exports = flatten;
