function each(array, cb) {
  if (arguments.length !== 2 || !Array.isArray(array)) {
    return [];
  }
  for (let index = 0; index < array.length; index++) {
    cb(array[index], index, array);
  }
}

module.exports = each;